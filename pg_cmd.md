Create DB

```
CREATE ROLE pgpool SUPERUSER CREATEDB CREATEROLE INHERIT REPLICATION LOGIN ENCRYPTED PASSWORD 'changeme';
CREATE USER repmgr SUPERUSER LOGIN ENCRYPTED PASSWORD 'changeme';
CREATE DATABASE repmgr OWNER repmgr;
\q
```
        
Sync DB

    ```
    /usr/pgsql-9.4/bin/repmgr -D /var/lib/pgsql/9.4/data -d repmgr -p 5432 -U repmgr -R postgres standby clone db1
    ```

register
1. register master

    ```
    /usr/pgsql-9.4/bin/repmgr -f /var/lib/pgsql/repmgr/repmgr.conf master register
    ```
        
2. register standby

    ```
    /usr/pgsql-9.4/bin/repmgr -f /var/lib/pgsql/repmgr/repmgr.conf standby register
    ```

Create and Change password

    ```
    rm /etc/pgpool-II-94/pool_passwd
    touch /etc/pgpool-II-94/pool_passwd 
    chown postgres:postgres /etc/pgpool-II-94/pool_passwd
    su - postgres -c "pg_md5 -m -u pgpool changeme"

    sleep 5
    rm /etc/pgpool-II-94/pcp.conf
    echo "pgpool:$(pg_md5 changeme)"|tee /etc/pgpool-II-94/pcp.conf
    chmod 644 /etc/pgpool-II-94/pcp.conf
    ```

Detach from PGPOOL

    ```
    pcp_detach_node -dw 0 -h localhost -p 9898 -U pgpool -W 'changeme' -n 1
    ```

pcp_node comnand

    ```
    pcp_node_count        - retrieves the number of nodes
    pcp_node_info         - retrieves the node information
    pcp_proc_count        - retrieves the process list
    pcp_proc_info         - retrieves the process information
    pcp_systemdb_info     - retrieves the System DB information
    pcp_detach_node       - detaches a node from pgpool-II
    pcp_attach_node       - attaches a node to pgpool-II
    pcp_stop_pgpool       - stops pgpool-II
    ```
